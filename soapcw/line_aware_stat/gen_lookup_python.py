import scipy.stats as st
import numpy as np
from scipy.integrate import quad
import pickle
import os

#--------------------------------------------------------------------------------
# Numerically finding the integral by integrating over SNR^2, where the variance depends on the standard deviation
#------------------------------------------------------------------------------------


class LineAwareStatistic:

    def __init__(self, powers, ndet=2 ,k=2, N=48, pvs=1, pvl=5, ratio=1,approx=True):

        self.ndet = ndet
        self.pvs = pvs
        self.pvl = pvl
        self.ratio = ratio
        self.powers = powers
        
        if ndet == 1:
            self.signoiseline,self.signoise,self.sigline,self.sig,self.noise,self.line = self.gen_lookup_one_det(powers,approx=approx,pvs=pvs,pvl=pvl,ratio=ratio)
        elif ndet == 2:
            self.signoiseline,self.signoise,self.sigline,self.sig,self.noise,self.line = self.gen_lookup_two_det(powers,powers,approx=approx,pvs=pvs,pvl=pvl,ratio=ratio)
            
    def chi2_sig(self,lamb,gs,k,N,pv):
        """
        returns the likelihood of two powers multiplied by the prior on the snr**2
        args
        -------
        lamb: float
            snr^2
        g1: float
            SFT power in detector 1
        g2: float
            SFT power in detector 2
        k: int
            number of degrees of freedom
        N: int
            Number of summed SFTS
        pv: float
            width of exponetial distribution of prior
        returns
        ---------
        func*prior: float
        likelihood multiplied by prior
        """
        if len(np.shape(gs)) == 0:
            func = st.ncx2.pdf(gs,df=N*k,nc=lamb,loc=0,scale=1)
        else:
            func = np.prod([st.ncx2.pdf(i,df=N*k,nc=lamb,loc=0,scale=1) for i in gs], axis=0)
        wid = 1./pv
        return func*wid*np.exp(-wid*lamb)


    def chi2_line(self,lamb,gs,k,N,pv):
        """
        returns the likelihood of two powers multiplied by the prior on the snr**2
        works for 1 or 2 detectors only

        args
        -------
        lamb: float
        snr^2
        g1: float
        SFT power in detector 1
        g2: float
        SFT power in detector 2
        k: int
        number of degrees of freedom
        N: int
        Number of summed SFTS
        pv: float
        width of exponetial distribution of prior
        returns
        ---------
        func*prior: float
        likelihood multiplied by prior
        """
        if len(np.shape(gs)) == 0:
            func = st.ncx2.pdf(gs,df=N*k,nc=lamb,loc=0,scale=1)
        else:
            func = 1./len(gs)*(st.chi2.pdf(gs[0],df=N*k,loc=0,scale=1)*st.ncx2.pdf(gs[1],df=N*k,nc=lamb,loc=0,scale=1) + st.ncx2.pdf(gs[0],df=N*k,nc=lamb,loc=0,scale=1)*st.chi2.pdf(gs[1],df=N*k,loc=0,scale=1))
        wid = 1./pv
        return func*wid*np.exp(-wid*lamb)
    
    
    def chi2_noise(self,gs,k,N):
        if len(np.shape(gs)) == 0:
            func = st.chi2.pdf(gs,df=N*k,loc=0,scale=1)
        else:
            func = np.prod([st.chi2.pdf(i,df=N*k,loc=0,scale=1) for i in gs],axis=0)
        return func


    def two_det(self,g1,g2,k,N,pvs=10,pvl=10,ratio=1., approx=True):
        """
        integrate likelihood and prior to get evidence for powers g1 and g2
        args
        -------
        g1: float
        g2: float
        k: int
        N: int
        pvs: float
        pvl: float
        ratio: float
        approx: bool
            choose to approximate the integral with trapz or full integration
        """
        if approx:
            l = np.linspace(0,100,500)
            sig_int = np.trapz(np.nan_to_num(self.chi2_sig(l,[g1,g2],k,N,pvs)),l)
            line_int = np.trapz(np.nan_to_num(self.chi2_line(l,[g1,g2],k,N,pvl)),l)
        else:
            sig_int, sig_err = quad(self.chi2_sig,0,np.inf, args = ([g1,g2],k,N,pvs))
            line_int, line_err = quad(self.chi2_line,0,np.inf, args = ([g1,g2],k,N,pvl))

        noise = self.chi2_noise([g1,g2],k,N)
        siglinenoise = sig_int/(ratio*line_int + noise )
        signoise = sig_int/noise
        sigline = sig_int/line_int
        
        return siglinenoise,signoise,sigline,sig_int,noise,line_int

    def one_det(self,g1,k,N,pvs=10,pvl=10,ratio=1., approx=True):
        """
        integrate likelihood and prior to get evidence for powers g1 and g2
        args
        -------
        approx: bool
            choose to approximate the integral with trapz or full integration
        """

        if approx:
            l = np.linspace(0,100,500)
            sig_int = np.trapz(np.nan_to_num(self.chi2_sig(l,g1,k,N,pvs)),l)
            line_int = np.trapz(np.nan_to_num(self.chi2_line(l,g1,k,N,pvl)),l)
        else:
            sig_int, sig_err = quad(self.chi2_sig,0,np.inf, args = (g1,k,N,pvs))
            line_int, line_err = quad(self.chi2_line,0,np.inf, args = (g1,k,N,pvl))

        noise = self.chi2_noise(g1,k,N)
        siglinenoise = sig_int/(ratio*line_int + noise )
        signoise = sig_int/noise
        sigline = sig_int/line_int
        
        return siglinenoise,signoise,sigline,sig_int,noise,line_int

    
    def gen_lookup_one_det(self,powers,approx=True,pvs=10,pvl=10,ratio=1):
        """
        calculate lookup table for values of x in the detector
        args
        -----------
        powers: array, list
            list of spectrogram powers to calcualte statistic at
        returns
        ---------
        signoiseline: log(sig/(noise+line))
        signoise: log(sig/(noise))
        sigline: log(sig/(line))
        sig: log(sig)
        noise: log(noise)
        line: log(line)
        """

        signoiseline = np.zeros((len(powers)))
        signoise = np.zeros((len(powers)))
        sigline = np.zeros((len(powers)))
        sig = np.zeros((len(powers)))
        noise = np.zeros((len(powers)))
        line = np.zeros((len(powers)))
        
        for i in range(len(powers)):
            ig = self.one_det(powers[i],2.,48.,pvs,pvl,ratio,approx=approx)
            signoiseline[i] = ig[0]
            signoise[i] = ig[1]
            sigline[i] = ig[2]
            sig[i] = ig[3]
            noise[i] = ig[4]
            line[i] = ig[4]
                    
        return signoiseline,signoise,sigline,sig,noise,line

    def gen_lookup_two_det(self,powers1,powers2,approx=True,pvs=10,pvl=10,ratio=1):
        """
        calculate lookup table for values of x and y in each detector
        args
        ----------
        powers1: array, list
            list of spectrogram powers to calcualte statistic at in det1
        powers2: array, list
            list of spectrogram powers to calcualte statistic at in det2


        returns
        ---------
        signoiseline: log(sig/(noise+line))
        signoise: log(sig/(noise))
        sigline: log(sig/(line))
        sig: log(sig)
        noise: log(noise)
        line: log(line)
        """

        signoiseline = np.zeros((len(powers1),len(powers2)))
        signoise = np.zeros((len(powers1),len(powers2)))
        sigline = np.zeros((len(powers1),len(powers2)))
        sig = np.zeros((len(powers1),len(powers2)))
        noise = np.zeros((len(powers1),len(powers2)))
        line = np.zeros((len(powers1),len(powers2)))
        
        for i in range(len(powers1)):
            for j in range(len(powers2)):
                if j > i:
                    continue
                ig = self.two_det(powers1[i],powers2[j],2.,48.,pvs,pvl,ratio,approx=approx)
                # symmetric matrix so set opposite elements to same, format it [(x1,x2),(y1,y2)]
                signoiseline[(i,j),(j,i)] = ig[0]
                signoise[(i,j),(j,i)] = ig[1]
                sigline[(i,j),(j,i)] = ig[2]
                sig[(i,j),(j,i)] = ig[3]
                noise[(i,j),(j,i)] = ig[4]
                line[(i,j),(j,i)] = ig[4]
                    
        return signoiseline,signoise,sigline,sig,noise,line


    def save_lookup(self,outdir,log=True):
        """
        save the lookup table for two detectors with the line aware statistic
        
        Args
        --------------
        outdir: string
        directory to save lookup table file
        pow_range: tuple
        ranges for the spectrogram power (lower, upper, number), default (1,400,500)
        
        """
        minimum,maximum,num = min(self.powers),max(self.powers),len(self.powers)

        fname = outdir+"/signoiseline_{}det_{}_{}_{}.txt".format(self.ndet,self.pvs,self.pvl,self.ratio)
        
        if os.path.isfile(fname):
            pass
        else:
            with open(fname,'wb') as f:
                header = "{} {} {}".format(minimum,maximum,num)
                if log:
                    np.savetxt(f,np.log(self.signoiseline),header = header)
                elif not log:
                    np.savetxt(f,self.signoiseline,header = header)


    def save_multiple(self,powerrange,pvsrange,pvlrange,ratiorange):

        pass
                    
#------------------------------------
# Line aware statistic with consistent amplitude
# --------------------------------------


class LineAwareAmpStatistic:

    def __init__(self, powers, ndet=2, fractions = None, k = 2, N = 48, pvs = 1, pvl = 5, ratio=1, approx=True):
        """

        """
        self.ndet = ndet
        self.pvs = pvs
        self.pvl = pvl
        self.ratio = ratio
        self.powers = powers
        self.fractions = fractions
        self.k = k
        self.N = N

        self.noisefunc = st.chi2(df=self.N*self.k,loc=0,scale=1)
        
        if ndet == 1:
            raise Exception("This function does not yet exist")
            self.signoiseline,self.signoise,self.sigline,self.sig,self.noise,self.line = self.gen_lookup_one_det(powers,fractions,approx=approx,pvs=pvs,pvl=pvl,ratio=ratio)
        elif ndet == 2:
            self.signoiseline,self.signoise,self.sigline,self.sig,self.noise,self.line = self.gen_lookup_two_det(powers,fractions, approx=approx,pvs=self.pvs,pvl=self.pvl,ratio=self.ratio)


    def chi2_sig(self,lamb,g1,g2,k,N,pv,fraction):
        """
        function to generate the signal probability as a function of the power in each detector. includes a factor which includes the noise disrtibution and duty cycle for each of the detectors. works for 2 detectors.
        args
        ---------
        lamb: float
            factor proportional to the SNR
        g1: float
            SFT power in detector 1
        g2: float
            SFT power in detector 2
        k: int
            number of degrees of freedom for chi2 distribution
        N: int
            number of summed SFTs 
        pv: float
            width of the exponentional distribution used for the prior on lamb
        fraction: float
            ratio of the detectors duty cycle and psd, \frac{S_2 f_1}{S_1 f_2}
        returns
        ----------
        prob: float
            probability of these two powers being a signal given the detector powers, duty cycles and noise psd
        """
        if fraction>=1:
            func = st.ncx2.pdf(g1,df=N*k,nc=lamb,loc=0,scale=1)*st.ncx2.pdf(g2,df=N*k,nc=lamb*fraction,loc=0,scale=1)
        else:
            func = st.ncx2.pdf(g1,df=N*k,nc=lamb/fraction,loc=0,scale=1)*st.ncx2.pdf(g2,df=N*k,nc=lamb,loc=0,scale=1)

        wid = 1./pv
        return func*wid*np.exp(-wid*lamb)

    def chi2_line(self,lamb,g1,g2,k,N,pv,fraction):
        """
        function to generate the line probability as a function of the power in each detector. includes a factor which includes the noise disrtibution and duty cycle for each of the detectors. works for 2 detectors.
        args
        ---------
        lamb: float
            factor proportional to the SNR
        g1: float
            SFT power in detector 1
        g2: float
            SFT power in detector 2
        k: int
            number of degrees of freedom for chi2 distribution
        N: int
            number of summed SFTs 
        pv: float
            width of the exponentional distribution used for the prior on lamb
        fraction: float
            ratio of the detectors duty cycle and psd, \frac{S_2 f_1}{S_1 f_2}
        returns
        ----------
        prob: float
            probability of these two powers being a noise line given the detector powers, duty cycles and noise psd
        """
        ch2_v = self.noisefunc.pdf([g1,g2])
        
        if fraction >= 1:
            func = ch2_v[0]*st.ncx2.pdf(g2,df=N*k,nc=lamb*fraction,loc=0,scale=1) + st.ncx2.pdf(g1,df=N*k,nc=lamb,loc=0,scale=1)*ch2_v[1]
        else:
            func = ch2_v[0]*st.ncx2.pdf(g2,df=N*k,nc=lamb,loc=0,scale=1) + st.ncx2.pdf(g1,df=N*k,nc=lamb/fraction,loc=0,scale=1)*ch2_v[1]

        wid = 1./pv
        return 0.5*func*wid*np.exp(-wid*lamb)

    def chi2_noise(self,g1,g2,k,N):
        """
        function to generate the noise probability as a function of the power in each detector.
        args
        ---------
        g1: float
            SFT power in detector 1
        g2: float
            SFT power in detector 2
        k: int
            number of degrees of freedom for chi2 distribution
        N: int
            number of summed SFTs 
        returns
        ----------
        prob: float
            probability of these two powers being noise given the detector powers
        """
        func = np.prod(self.noisefunc.pdf([g1,g2]))
        return func
    
    def line_aware_amp(self,g1,g2,fraction,k,N,pvs=10,pvl=10,ratio=1.):
        """
        marginalises over the "SNR" for both the noise and line case and combines into an odds ratio(using quad)
        args
        ---------
        g1: float
            SFT power in detector 1
        g2: float
            SFT power in detector 2
        k: int
            number of degrees of freedom for chi2 distribution
        N: int
            number of summed SFTs 
        pvs: float
            width of the exponentional distribution used for the prior on lamb in the signal case
        pvl: float
            width of the exponentional distribution used for the prior on lamb in the line case
        fraction: float
            ratio of the detectors duty cycle and psd, \frac{S_2 f_1}{S_1 f_2}
        ratio: float
            ratio of the prior on the line model to the gaussian noise model, \frac{M_{N}}{M_{G}}
        returns
        ----------
        bsgl1: float
            odds ratio of all three models \frac{M_{S}}{M_{L} + M_{G}}
        bsgl2: float
            odds ratio of signal and gaussian noise models \frac{M_{S}}{M_{G}}
        bsgl3: float
            odds ratio of signal and line models \frac{M_{S}}{M_{L}}
        sig_int: float
            likelihood of signal model
        noise_func: float
            likelihood of gaussian noise model
        line_int: float
            likelihood of line model
        """
        noise = self.chi2_noise(g1,g2,k,N)
        sig_int, sig_err = quad(self.chi2_sig,0,np.inf, args = (g1,g2,k,N,pvs,fraction))
        line_int, line_err = quad(self.chi2_line,0,np.inf, args = (g1,g2,k,N,pvl,fraction))
        bsgl1 = sig_int/(ratio*line_int + noise)
        bsgl2 = sig_int/(noise)
        bsgl3 = sig_int/line_int
        return bsgl1,bsgl2,bsgl3,sig_int,noise,line_int

    def line_aware_amp_approx(self,g1,g2,fraction,k,N,pvs=1,pvl=1,ratio=1.):
        """
        marginalises over the "SNR" for both the noise and line case and combines into an odds ratio (approximate using trapz)
        args
        ---------
        g1: float
            SFT power in detector 1
        g2: float
            SFT power in detector 2
        k: int
            number of degrees of freedom for chi2 distribution
        N: int
            number of summed SFTs 
        pvs: float
            width of the exponentional distribution used for the prior on lamb in the signal case
        pvl: float
            width of the exponentional distribution used for the prior on lamb in the line case
        fraction: float
            ratio of the detectors duty cycle and psd, \frac{S_2 f_1}{S_1 f_2}
        ratio: float
            ratio of the prior on the line model to the gaussian noise model, \frac{M_{N}}{M_{G}}
        returns
        ----------
        bsgl1: float
            odds ratio of all three models \frac{M_{S}}{M_{L} + M_{G}}
        bsgl2: float
            odds ratio of signal and gaussian noise models \frac{M_{S}}{M_{G}}
        bsgl3: float
            odds ratio of signal and line models \frac{M_{S}}{M_{L}}
        sig_int: float
            likelihood of signal model
        noise_func: float
            likelihood of gaussian noise model
        line_int: float
            likelihood of line model
        """
        l = np.linspace(0,100,500)
        noise = self.chi2_noise(g1,g2,k,N)
        sig_int = np.trapz(np.nan_to_num(self.chi2_sig(l,g1,g2,k,N,pvs,fraction)),l)
        line_int = np.trapz(np.nan_to_num(self.chi2_line(l,g1,g2,k,N,pvl,fraction)),l)
        signoiseline = sig_int/(ratio*line_int + noise)
        signoise = sig_int/noise
        sigline = sig_int/line_int
        return signoiseline,signoise,sigline,sig_int,noise,line_int

    def line_aware_amp_cpp(self,g1,g2,logfrac,k,N,pvs=1,pvl=1,ratio=1.):
        """
        marginalises over the "SNR" for both the noise and line case and combines into an odds ratio (approximate using trapz)
        args
        ---------
        g1: float
            SFT power in detector 1
        g2: float
            SFT power in detector 2
        k: int
            number of degrees of freedom for chi2 distribution
        N: int
            number of summed SFTs 
        pvs: float
            width of the exponentional distribution used for the prior on lamb in the signal case
        pvl: float
            width of the exponentional distribution used for the prior on lamb in the line case
        logfrac: float
            ratio of the detectors duty cycle and psd, \frac{S_2 f_1}{S_1 f_2}
        ratio: float
            ratio of the prior on the line model to the gaussian noise model, \frac{M_{N}}{M_{G}}
        returns
        ----------
        bsgl1: float
            log odds ratio of all three models \frac{M_{S}}{M_{L} + M_{G}}
        bsgl2: float
            log odds ratio of signal and gaussian noise models \frac{M_{S}}{M_{G}}
        bsgl3: float
            log odds ratio of signal and line models \frac{M_{S}}{M_{L}}
        sig_int: float
            log likelihood of signal model
        noise_func: float
            log likelihood of gaussian noise model
        line_int: float
            log likelihood of line model
        """
        sig_int = integrals.Integral([g1],g2,logfrac,k,N,pvs,"signal")
        line_int = integrals.Integral([g1],g2,logfrac,k,N,pvl,"line")
        bsgl1 = sig_int/(ratio*line_int + chi2_noise(g1,g2,k,N))
        bsgl2 = sig_int/(chi2_noise(g1,g2,k,N))
        bsgl3 = sig_int/line_int
        return np.log(bsgl1),np.log(bsgl2),np.log(bsgl3),np.log(sig_int),np.log(chi2_noise(g1,g2,k,N)),np.log(line_int)

    
    def gen_lookup_two_det(self,powers,fractions,approx=True,pvs=1,pvl=1,ratio=1):
        """
        calculates the odds ratio in ch_integrals_approx_noise for given values of power and ratio.
        args
        --------
        x: array
            array of the SFT power in detector 1
        y: array
            array of SFT power in detector 2
        n1: array
            array of the ratios of the duty cycle and noise psds, \frac{f_{1} S_{2}}{f_{2} S_{1}}
        int_type: string
            which statistic to use ("chi2" only option for now)
        approx: bool
            choose whether to use trapz or quad to complete integrals
        pvs: float
            width on prior for signal model
        pvl: float
            width on prior for line model
        ratio: float
            ratio of the priors on the line and signal models
        returns
        ----------
        val: array
            array of odd ratio with 3 models
        val1: array
            array of odd ratio with signal/noise model
        val2: array
            array of odd ratio with signal/line model
        val3: array
            array of signal likelihoods
        val4: array
            array of noise likelihoods
        val: array
            array of line likelihoods
        """
        signoiseline = np.zeros((len(fractions),len(powers),len(powers)))
        signoise = np.zeros((len(fractions),len(powers),len(powers)))
        sigline = np.zeros((len(fractions),len(powers),len(powers)))
        sig = np.zeros((len(fractions),len(powers),len(powers)))
        noise = np.zeros((len(fractions),len(powers),len(powers)))
        line = np.zeros((len(fractions),len(powers),len(powers)))
        for i in range(len(powers)):
            for j in range(len(powers)):
                if j > i:
                    continue
                for k in range(len(fractions)):
                    if approx:
                        las = self.line_aware_amp_approx(powers[i],powers[j],fractions[k],self.k,self.N,pvs,pvl,ratio)
                    else:
                        las = self.line_aware_amp(powers[i],powers[j],fractions[k],self.k,self.N,pvs,pvl,ratio)

                    signoiseline[(k,k),(i,j),(j,i)] = las[0]
                    signoise[(k,k),(i,j),(j,i)] = las[1]
                    sigline[(k,k),(i,j),(j,i)] = las[2]
                    sig[(k,k),(i,j),(j,i)] = las[3]
                    noise[(k,k),(i,j),(j,i)] = las[4]
                    line[(k,k),(i,j),(j,i)] = las[5]
                    
        return signoiseline,signoise,sigline,sig,noise,line
            
    def gen_lookup_cpp(self,g1,g2,logfrac,pvs,pvl,ratio):
        """
        calculates the odds ratio in ch_integrals_approx_noise for given values of power and ratio.
        args
        --------
        g1: array
            array of the SFT power in detector 1
        g1: array
            array of SFT power in detector 2
        logfrac: array
            array of the ratios of the duty cycle and noise psds, \frac{f_{1} S_{2}}{f_{2} S_{1}}
        pvs: float
            width on prior for signal model
        pvl: float
            width on prior for line model
        ratio: float
            ratio of the priors on the line and signal models
        returns
        ----------
        val: array
            array of odd ratio with 3 models
        val1: array
            array of signal likelihoods
        val2: array
            array of line likelihoods
        val3: array
            array of noise likelihoods
        """
        return np.log(integrals.Integral(list(g1),list(g2),list(logfrac),2,48,pvs,pvl,ratio))


    def save_lookup(self,outdir,stat_type="signoiseline",log=True):
        """
        save the lookup table for two detectors with the line aware statistic
        
        Args
        --------------
        outdir: string
        directory to save lookup table file
        pow_range: tuple
        ranges for the spectrogram power (lower, upper, number), default (1,400,500)
        
        """
        minimum,maximum,num = min(self.powers),max(self.powers),len(self.powers)
        minimum_frac,maximum_frac,num_frac = min(self.fractions),max(self.fractions),len(self.fractions)

        fname = outdir+"/{}amp_{}det_{}_{}_{}.txt".format(stat_type,self.ndet,self.pvs,self.pvl,self.ratio)
        
        if os.path.isfile(fname):
            pass
        else:
            with open(fname,'wb') as f:
                header = "{} {} {}".format(minimum,maximum,num)
                if log:
                    np.savetxt(f,np.log(getattr(self,stat_type)),header = header)
                elif not log:
                    np.savetxt(f,getattr(self,stat_type),header = header)



                
