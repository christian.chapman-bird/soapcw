====
soap
====


.. image:: https://img.shields.io/pypi/v/soap.svg
        :target: https://pypi.python.org/pypi/soap

.. image:: https://img.shields.io/travis/jcbayley/soap.svg
        :target: https://travis-ci.org/jcbayley/soap

.. image:: https://readthedocs.org/projects/soap-cw/badge/?version=latest
        :target: https://soap-cw.readthedocs.io
        :alt: Documentation Status

SOAP: Applying the Viterbi algorithm to search for continuous sources
of gravitational waves.

SOAP is primarily developed to search for continuous sources of
gravitational waves, however, has a more general application to search
for and long duration weak signal.


* Free software: MIT license
* Documentation: https://soap-cw.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
