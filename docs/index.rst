Welcome to soap's documentation!
======================================

SOAP is a fast algorithm developed to search for coninuous sources of gravitational waves. However, has uses in searching for any long duration weak signal.

Setup
===========
.. toctree::
    :maxdepth: 1

    readme
    installation

Usage
=============

.. toctree::
    :maxdepth: 2
   
    usage/index

SOAP search
===========

.. toctree::
    :maxdepth: 2

    viterbialgorithm.ipynb
    transitionmatrix.ipynb
    bayesianlineaware.ipynb

Info
=========

.. toctree::
    :maxdepth: 1 

    modules
    contributing
    authors
    history
   

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
